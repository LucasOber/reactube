import React, { useState } from 'react';
import {Button, Form, FormControl} from "react-bootstrap";
import api from '../lib/api';
import {useHistory} from "react-router-dom";

const SearchBar = ({onResults, typeSearch}) => {
    const [typed, setTyped] = useState(''); 
    const history = useHistory();

    const search = async () => {
        const resp = await api.get('/search', {
            params: {
                q: typed,
                part: 'snippet',
                maxResults: 10, 
                type: typeSearch,
            }
        });
        console.log('Received', resp.data.items);
        onResults(resp.data.items);
        history.push(`/search/${typeSearch}/${typed}`);
    };

    return (<Form inline>
    <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={event => setTyped(event.target.value)}/>
    <Button variant="outline-info" onClick={search}>Search</Button>
    </Form>)
}

export default SearchBar;