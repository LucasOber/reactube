import React, {useState} from 'react';
import Container from 'react-bootstrap/Container';
import Result from './components/Result';
import MyNav from './components/MyNav';
import DetailedVideo from './components/DetailedVideo';
import './App.css';

import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
  const [results, setResults] = useState([]);
  const [selectedVideo, selectVideo] = useState(null);
  const [typeSearch, changeType] = useState("video");

  return (
    <Container className="p-3">
    <Router>
      <MyNav onResults={setResults} changeType={changeType} typeSearch={typeSearch}/>
      <Switch>
        <Route path="/videos/:videoId">
          {selectedVideo != null && 
          <DetailedVideo id={selectVideo.id}
            snippet={selectedVideo.snippet}
            statistics={selectedVideo.statistics}
            player={selectedVideo.player}
            onEscape={() => selectVideo(null)}/>}
        </Route>
        <Route path="/search/:type/:search">
        <>
        {results.map(r => {
            <Result queryResult={r} type={typeSearch}/>
        })}
        </>
        </Route>
      <Route path="/">
        {results.length === 0 && selectedVideo == null && "Merci d'effectuer une recherche..."}
      </Route>
    </Switch>
    </Router>​​
  </Container>
  );
}

export default App;