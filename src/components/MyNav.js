import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from "./SearchBar";
const MyNav = ({onResults, changeType, typeSearch}) => {

    function changeSearchType(type){
      changeType(type)
      console.log(typeSearch);
    }

    return(
    <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Reactube</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link onClick={() => changeSearchType("video")}>Videos</Nav.Link>
          <Nav.Link onClick={() => changeSearchType("channel")}>Channels</Nav.Link>
        </Nav>
        <SearchBar onResults={onResults} typeSearch={typeSearch}/>
      </Navbar>
      );
}
export default MyNav;