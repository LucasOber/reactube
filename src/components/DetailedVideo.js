import React from "react";
import {Image, Card, Row, Col, Badge, Button} from 'react-bootstrap';

const DetailedVideo = ({id, snippet, statistics, player, onEscape}) => {

    const {title, channelTitle, description, publishTime, tags} = snippet;
    const {viewCount, likeCount, dislikeCount, commentCount} = statistics;
    
    return(
        <Card>
            <Card.Body>
            <Button variant="danger" style={{float: 'right'}} onClick={onEscape}>Retour</Button>
            <Row>
                <Col width={"100%"}>
                    <div dangerouslySetInnerHTML={{
                        __html: player.embedHtml
                            .replace(/width="\d+"/, 'width="100%"')
                            .replace(/height="\d+"/, 'height="640"')
                    }}/>
                </Col>
            </Row>
            <Row>
                <h5 className="tags">{tags.map(tag => <><Badge variant="dark" pill>{tag}</Badge>{' '}</>)}</h5>
            </Row>
            <Row>
                <Col>
                    <h2>{title}</h2>
                    <h5 className="mb-2 text-info">{channelTitle}</h5>
                    <h5 className="mb-2 text-muted">
                        {viewCount} ▶️
                        - {likeCount} 👍
                        - {dislikeCount} 👎
                        - {commentCount} 💬
                    </h5>
                    <Card.Text dangerouslySetInnerHTML={{__html: description.replace(/\n/g, '<br/>')}}/>
                    <Card.Text className="mb-1 text-muted">{publishTime}</Card.Text>
                </Col>
            </Row>
            </Card.Body>
        </Card>
    );
}

export default DetailedVideo;