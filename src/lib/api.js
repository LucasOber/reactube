import axios from 'axios'

const key = 'AIzaSyB097yaJF_KVzd5jdczGSuFICrVFEGmPZ4'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})

export default fetcher;